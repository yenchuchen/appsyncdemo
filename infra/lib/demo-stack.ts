import * as cdk from '@aws-cdk/core';
import * as appsync from '@aws-cdk/aws-appsync';
import * as lambda from '@aws-cdk/aws-lambda';
import * as ddb from '@aws-cdk/aws-dynamodb';

export class DemoStack extends cdk.Stack {

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    this.createGraphqlApi();
  }

  private createGraphqlApi() {

    // create AppSync graphql api
    const api = new appsync.GraphqlApi(this, 'YcDemoAPI', {
      name: 'YcDemoAPI',
      
      // set graphql schema location
      schema: appsync.Schema.fromAsset('../appsync/schema.graphql'),
      
      // set authorizer
      authorizationConfig: {
        defaultAuthorization: {
          authorizationType: appsync.AuthorizationType.API_KEY,
          apiKeyConfig: {
            name: 'default',
            description: 'default auth mode'
          },
        },
      },
      
      // enable tracing
      xrayEnabled: true,

      // enable logging
      logConfig: {
        excludeVerboseContent: false,
        fieldLogLevel: appsync.FieldLogLevel.ALL,
      }
    })

    // create lambda resolvers wo vtl template
    this.createNotesResolver(api);

    // create dynamodb resolvers with vtl template
    this.createTodoResolver(api);

    // a hello world lambda resolver for demo
    this.createDemoResolver(api);

    // output created resource info
    new cdk.CfnOutput(this, 'Region', { value: this.region })
    new cdk.CfnOutput(this, 'GraphqlId', { value: api.apiId })
    new cdk.CfnOutput(this, 'GraphqlEndpoint', { value: api.graphqlUrl })
    new cdk.CfnOutput(this, 'GraphqlApiKey', { value: api.apiKey || '' })
  }

  private createNotesResolver(api: appsync.GraphqlApi) {

    // create a lambda function
    const handler = new lambda.Function(this, 'NotesHandler', {
      runtime: lambda.Runtime.NODEJS_12_X,
      code: lambda.Code.fromAsset('../lambda/notes'),
      handler: 'main.handler',
      memorySize: 512
    });

    // create a dynamodb table
    const table = new ddb.Table(this, 'NotesTable', {
      billingMode: ddb.BillingMode.PAY_PER_REQUEST,
      partitionKey: {
        name: 'id',
        type: ddb.AttributeType.STRING,
      },
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    // grant table access permision to lambda
    table.grantFullAccess(handler)

    // set environment vars of lambda
    handler.addEnvironment('NOTES_TABLE', table.tableName);

    // AppSync related config
    // create lambda datasource
    const dataSource = api.addLambdaDataSource('NotesSource', handler);

    // create resolvers for the lambda datasource
    // typeName and fieldName should map to 'Query' and 'Mutation' declaration of schema.graphql
    dataSource.createResolver({
      typeName: "Query",
      fieldName: "getNoteById"
    });

    dataSource.createResolver({
      typeName: "Query",
      fieldName: "listNotes"
    });

    dataSource.createResolver({
      typeName: "Mutation",
      fieldName: "createNote"
    });

    dataSource.createResolver({
      typeName: "Mutation",
      fieldName: "deleteNote"
    });

    dataSource.createResolver({
      typeName: "Mutation",
      fieldName: "updateNote"
    });
  }

  private createTodoResolver(api: appsync.GraphqlApi) {

    const table = new ddb.Table(this, 'TodoTable', {
      partitionKey: { 
        name: 'id', 
        type: ddb.AttributeType.STRING 
      },
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    })

    // AppSync related config
    // create dynamodb datasource
    const dataSource = api.addDynamoDbDataSource('TodoSource', table)

    const vtlDir = '../appsync/resolvers/todo'

    // create resolvers for the lambda datasource
    // requestMappingTemplate and responseMappingTemplate should be provided to process request and response
    dataSource.createResolver({
      typeName: 'Mutation',
      fieldName: 'createTodo',
      requestMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Mutation.createTodo.req.vtl'
      ),
      responseMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Mutation.createTodo.res.vtl'
      )
    })

    dataSource.createResolver({
      typeName: 'Mutation',
      fieldName: 'updateTodo',
      requestMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Mutation.updateTodo.req.vtl'
      ),
      responseMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Mutation.updateTodo.res.vtl'
      )
    })

    dataSource.createResolver({
      typeName: 'Mutation',
      fieldName: 'deleteTodo',
      requestMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Mutation.deleteTodo.req.vtl'
      ),
      responseMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Mutation.deleteTodo.res.vtl'
      )
    })

    dataSource.createResolver({
      typeName: 'Query',
      fieldName: 'getTodo',
      requestMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Query.getTodo.req.vtl'
      ),
      responseMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Query.getTodo.res.vtl'
      )
    })

    dataSource.createResolver({
      typeName: 'Query',
      fieldName: 'listTodos',
      requestMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Query.listTodos.req.vtl'
      ),
      responseMappingTemplate: appsync.MappingTemplate.fromFile(
        vtlDir + '/Query.listTodos.res.vtl'
      )
    })
  }

  private createDemoResolver(api: appsync.GraphqlApi) {

    const handler = new lambda.Function(this, 'DemoHandler', {
      runtime: lambda.Runtime.NODEJS_12_X,
      code: lambda.Code.fromAsset('../lambda/demo'),
      handler: 'index.handler',
    })

    const dataSource = api.addLambdaDataSource(
      'DemoSource',
      handler
    )

    dataSource.createResolver({
      typeName: 'Query',
      fieldName: 'hello',
    })
  }
}
