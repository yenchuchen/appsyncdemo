import * as cdk from '@aws-cdk/core';
import * as cognito from '@aws-cdk/aws-cognito';
import * as lambda from '@aws-cdk/aws-lambda';
import { NodejsFunction, NodejsFunctionProps } from '@aws-cdk/aws-lambda-nodejs';
import { Duration } from '@aws-cdk/core';
import * as path from 'path';

export interface CognitoStackProps extends cdk.StackProps {
    //userPoolName: string;
}

export class CognitoStack extends cdk.Stack {

    public readonly userPool: cognito.UserPool;

    public readonly userPoolClient: cognito.UserPoolClient;

    constructor(scope: cdk.Construct, id: string, props?: CognitoStackProps) {

        super(scope, id, props);

        this.userPool = this.createUserPool(props);

        this.userPoolClient = this.createUserPoolClient(this.userPool, props);

        new cdk.CfnOutput(this, 'UserPoolId', { value: this.userPool.userPoolId });

        new cdk.CfnOutput(this, 'userPoolClientId', { value: this.userPoolClient.userPoolClientId })
    }

    private createUserPool(props?: CognitoStackProps): cognito.UserPool {

        const postAccountConfirmationTrigger = this.createNodejsFunction(
            this,
            'post-confirmation',
            {
                handler: 'main',
                entry: path.join(
                    __dirname,
                    '../../lambda/cognito-triggers/post-confirmation/index.ts',
                ),
            }
        );

        const preTokenGenerationTrigger = this.createNodejsFunction(
            this,
            'pre-token-generation',
            {
                handler: 'main',
                entry: path.join(
                    __dirname,
                    '../../lambda/cognito-triggers/pre-token-generation/index.ts',
                )
            },
        );

        return new cognito.UserPool(this, 'UserPool', {
            userPoolName: 'DevUserPool',
            selfSignUpEnabled: true,
            signInCaseSensitive: false,
            signInAliases: {
                email: true,
            },
            autoVerify: {
                email: true,
            },
            standardAttributes: {
                email: {
                    mutable: true,
                    required: true
                },
                phoneNumber: {
                    mutable: true,
                    required: false
                }
            },
            passwordPolicy: {
                minLength: 8,
                requireLowercase: true,
                requireUppercase: true,
                requireDigits: true,
                requireSymbols: false,
                tempPasswordValidity: Duration.days(7)
            },
            mfa: cognito.Mfa.OPTIONAL,
            accountRecovery: cognito.AccountRecovery.EMAIL_AND_PHONE_WITHOUT_MFA,
            removalPolicy: cdk.RemovalPolicy.DESTROY,
            lambdaTriggers: {
                postConfirmation: postAccountConfirmationTrigger,
                preTokenGeneration: preTokenGenerationTrigger,
            },
        });
    }

    private createUserPoolClient(userPool: cognito.UserPool, props?: CognitoStackProps): cognito.UserPoolClient {

        const standardCognitoAttributes = {
            email: true,
            emailVerified: true,
            phoneNumber: true,
            phoneNumberVerified: true,
        };

        const clientReadAttributes = new cognito.ClientAttributes()
            .withStandardAttributes(standardCognitoAttributes);

        const clientWriteAttributes = new cognito.ClientAttributes()
            .withStandardAttributes({
                ...standardCognitoAttributes,
                emailVerified: false,
                phoneNumberVerified: false,
            });

        return new cognito.UserPoolClient(this, 'UserPoolClient', {
            userPool,
            authFlows: {
                adminUserPassword: true,
                userSrp: true,
                userPassword: true,
            },
            readAttributes: clientReadAttributes,
            writeAttributes: clientWriteAttributes,
        });
    }

    private createNodejsFunction(scope: cdk.Construct, id: string, props: NodejsFunctionProps): NodejsFunction {

        return new NodejsFunction(
            scope,
            id,
            {
                handler: props.handler,
                entry: props.entry,
                runtime: props.runtime || lambda.Runtime.NODEJS_12_X,
                memorySize: props.memorySize || 128,
                timeout: props.timeout || cdk.Duration.seconds(30),
                bundling: {
                    externalModules: ['aws-sdk']
                },
            },
        );
    }
}
