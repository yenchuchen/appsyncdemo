#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { DemoStack } from '../lib/demo-stack';
import { CognitoStack } from '../lib/cognito-stack';

const app = new cdk.App();

// us-east-2 asia-atsoul-dev
const account = process.env.CDK_DEFAULT_ACCOUNT;
const region = process.env.CDK_DEFAULT_REGION;

new CognitoStack(app, 'CognitoStack', {
  env: {account: account, region: region}
});

new DemoStack(app, 'DemoStack', {});
