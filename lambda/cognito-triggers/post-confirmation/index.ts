import { Callback, Context, PostConfirmationTriggerEvent } from 'aws-lambda';

export async function main(
  event: PostConfirmationTriggerEvent,
  _context: Context,
  callback: Callback,
): Promise<void> {

  console.log('post confirmation event: ', JSON.stringify(event, null, 2));
  try {
    return callback(null, event);
  } catch (error) {
    return callback(error, event);
  }
}
