import { Callback, Context, PreTokenGenerationTriggerEvent } from 'aws-lambda';

export async function main(
  event: PreTokenGenerationTriggerEvent,
  _context: Context,
  callback: Callback,
): Promise<void> {

  console.log('pre token generation event: ', JSON.stringify(event, null, 2));
  try {
    event.response.claimsOverrideDetails = { "claimsToAddOrOverride": { "role": "developer" } };
    return callback(null, event);
  } catch (error) {
    return callback(error, event);
  }
}
/*
{
    "version": "1",
    "triggerSource": "TokenGeneration_Authentication",
    "region": "ap-northeast-1",
    "userPoolId": "ap-northeast-1_F3JfexD39",
    "userName": "0de96b90-e5fa-47b8-9ee3-cb2fc35342b1",
    "callerContext": {
        "awsSdkVersion": "aws-sdk-unknown-unknown",
        "clientId": "5rtcto9q2a2gg668i430vkj478"
    },
    "request": {
        "userAttributes": {
            "sub": "0de96b90-e5fa-47b8-9ee3-cb2fc35342b1",
            "cognito:email_alias": "yenchuchen@johnsonfitness.com",
            "cognito:user_status": "CONFIRMED",
            "email_verified": "true",
            "email": "yenchuchen@johnsonfitness.com"
        },
        "groupConfiguration": {
            "groupsToOverride": [],
            "iamRolesToOverride": [],
            "preferredRole": null
        }
    },
    "response": {
        "claimsOverrideDetails": null
    }
}
*/