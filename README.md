# AppSync and CDK demo
This is a demo project for AppSync with CDK.

## Directory Structure

 * `appsync`  
   schema.graphql and VTL template files.
 * `fn`  
    Lambda functions.
 * `infra`  
   CDK project.

